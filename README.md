# DIY Fermenter

This project allows you to control the temperature of a mini-fridge with an Arduino, and optionally allows you to log the state with a Python script (in my case, running on a connected Raspberry Pi).

This was very much inspired by the [DIY BrewPi project](http://diybrewpi.wikia.com/wiki/DIYBrewPi_Wikia), but I chose to go my own way for a few reasons:

1. I wanted to make sure I understood the entire system.
1. I thought the BrewPi project was overkill for what I wanted to accomplish.

## How it works


I used the [setup from the DIY BrewPi project](http://diybrewpi.wikia.com/wiki/DIYBrewPi_Wikia). In short:

- A personal heater is put in a mini-fridge
- The power to the heater and mini-fridge are connected to an outlet
- An Arduino measures the temperature and controls the outlet via a relay board
- The Arduino sends output via its serial port
- A Raspberry Pi is connected to the Arduino that reads the serial data and logs to a CSV

### Physical Setup

See the [schema from the DIY BrewPi project](https://imgur.com/a/Nval6). My changes:

- Connected a Raspberry Pi to the Arduino
- I used a breadboard in place of the "Eurostyle" connector

### Arduino

The code flow goes something like this:

Setup:

1. Find all of the sensors
1. Set the relay so that both outputs (heating and cooling) are off

Loop:

1. Check if there are any sensors; if not, go back to setup.
1. Print status to serial
1. Check current temperature and change control appropriately
1. Delay one second
1. Loop

All of the configuration -- which pins to use, temperature target and bound, etc -- are set as `DEFINE` statements at the top of the code.

### Python logging script (Raspberry Pi)

Similar to the Arduino setup, all of the configuration is at the top of the file.

This is pretty simple -- it reads in the serial data from the Arduino, includes a time stamp, and writes to a text file. One "trick" is getting it to continue running after disconnecting; I just detach a tmux session.


## TODO

- Alert for failure
- Add multiple sensors and discard bad readings
- Make heating/cooling robust to bad readings
- Tune for stability and efficiency

