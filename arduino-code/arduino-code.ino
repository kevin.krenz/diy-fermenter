#include <OneWire.h>
#include <DallasTemperature.h>

#define ONEWIRE_BUS 10
#define MAX_NUM_SENSORS 5

#define COOL_PIN 5
#define HEAT_PIN 6

#define TEMP_TARGET 46
#define TEMP_ERROR 1

#define WINDOW 10

int num_sensors = 0;
DeviceAddress addresses[MAX_NUM_SENSORS];

OneWire onewire(ONEWIRE_BUS);
DallasTemperature sensors(&onewire);

int temp_change_counter = 0;
int temp_control_status = 0;

// Search for and set up temperature sensors.
void setup(void) {

  // Find the sensors
  byte addr[8];

  Serial.begin(9600);
  onewire.reset_search();

  Serial.println("Starting search");
  while (onewire.search(addr) && (num_sensors < MAX_NUM_SENSORS)) {
      memcpy(addresses[num_sensors++], addr, sizeof(byte) * 8);
  }

  if (num_sensors == 0) {
    Serial.println("No sensors found!");
  } 
  else {
    Serial.print("Number of sensors found: ");
    Serial.print(num_sensors);
    Serial.println();
  }

  // Set up the output pins and turn them off
  pinMode(COOL_PIN, OUTPUT);
  digitalWrite(COOL_PIN, HIGH);

  pinMode(HEAT_PIN, OUTPUT);
  digitalWrite(HEAT_PIN, HIGH);

  return;
}


// Verify that there is at least one temperature sensor.
// Occasionally check for more.
// Read temperature and toggle heat/cold appropriately.
void loop(void) {

  int i;

  // Check if the search needs to be reset
  if (num_sensors == 0) {
    Serial.println("No addresses found. Resetting search.");
    delay(1000);
    setup();
    return;
  }

  // Write the status of heating/cooling
  Serial.print(TEMP_TARGET);
  Serial.print(",");
  Serial.print(TEMP_ERROR);
  Serial.print(",");
  Serial.print(temp_control_status);
  Serial.print(",");

  // Read & print temperature for each sensor
  float temp_c, temp_f, temp_avg, temp_diff;

  temp_avg = 0;

  for (i = 0; i < num_sensors; i++) {

    // Request a temperature reading
    sensors.requestTemperaturesByAddress(addresses[i]);

    // Obtain reading and conver to Fahrenheit
    temp_c = sensors.getTempC(addresses[i]);
    temp_f = DallasTemperature::toFahrenheit(temp_c);
    temp_avg += temp_f;
    
    // Print the temperature reading
    Serial.print(temp_f);

    // Print either a comma or a newline
    if (i == (num_sensors - 1)) {
      Serial.println();
    }
    else {
      Serial.print(",");
    }


    // Temperature control

    // Avoid cooling/heating sources rapidly turning off & on
    if (temp_change_counter > 0) {
      temp_change_counter--;
      return;
    }

    temp_avg = temp_avg / num_sensors;
    temp_diff = temp_avg - TEMP_TARGET;
    
    if (temp_diff < TEMP_ERROR * -1) {
      digitalWrite(COOL_PIN, HIGH);
      digitalWrite(HEAT_PIN, LOW);
      temp_change_counter = WINDOW / 2; // Heating is faster
      temp_control_status = 1;
    }

    else if (temp_diff > TEMP_ERROR) {
      digitalWrite(COOL_PIN, LOW);
      digitalWrite(HEAT_PIN, HIGH);
      temp_change_counter = WINDOW;
      temp_control_status = -1;
    }

    else {
      digitalWrite(COOL_PIN, HIGH);
      digitalWrite(HEAT_PIN, HIGH);
      temp_control_status = 0;
    }

  }

  delay(1000);

}
