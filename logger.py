import datetime
import serial
import time

# Configure these!
port = '/dev/ttyACM0'
baudrate = 9600
filename = 'brewpi'

# Try opening the port
# If it fails, wait 10 seconds and try again
while True:
    try:
        arduino = serial.Serial(port, baudrate)
        print('Opened ' + port + ' with a baud rate of ' + str(baudrate))
        break
    except:
        print('Failed to open ' + port + '. Will try again in 10 seconds.')
        time.sleep(10)

# Connection open!

## Open a file
full_filename = filename + str(datetime.datetime.now()) + '.csv'
output_file = open(full_filename, 'a')

while True:
    data = arduino.readline()
    timestamp = str(datetime.datetime.now())
    output_file.write(','.join([timestamp,data]))
    output_file.flush()

